Name:           ukui-screensaver
Version:        4.0.0.0
Release:        2
Summary:        Screensaver for UKUI desktop environment
License:        GPL-3+ and GPL-2+
URL:            http://www.ukui.org
Source0:        %{name}-%{version}.tar.gz

Patch01:	0001-ukui-screensaver-resolve-compilation-errors.patch
Patch02:        0001-fix-ukui-screensaver-4.0.0.0-about-ukui-screensaver-qt.patch

BuildRequires:  cmake
BuildRequires:  qt5-qtbase-devel
BuildRequires:  qt5-qtx11extras-devel
BuildRequires:  pam-devel
BuildRequires:  qt5-qttools-devel
BuildRequires:  qt5-qttools-devel
BuildRequires:  glib2-devel 
BuildRequires:  opencv
BuildRequires:  libX11-devel
BuildRequires:  libXtst-devel
BuildRequires:  qt5-qtsvg-devel
BuildRequires:  libkylin-nm-base
BuildRequires:  NetworkManager-libnm-devel
BuildRequires:  kf5-networkmanager-qt-devel
BuildRequires:  kylin-nm-plugin
BuildRequires:  kf5-kwayland-devel
BuildRequires:  libkscreen-qt5-devel
BuildRequires:  gsettings-qt-devel
BuildRequires:  kf5-kwindowsystem-devel
BuildRequires:  libmatemixer-devel
BuildRequires:  imlib2-devel
BuildRequires:  ukui-interface
BuildRequires:  libkysdk-sysinfo-devel
BuildRequires:  ukui-input-gather
BuildRequires:  libinput-devel
BuildRequires:  libukuiinputgatherclient-devel
BuildRequires:  layer-shell-qt-devel


Requires:       ukui-session-manager
Requires:       glib2-devel
Requires:       ethtool
Requires:       mate-common
Requires:       libpam-biometric
Requires:       ukui-input-gather
Requires:       layer-shell-qt


%description
A simple and lightweight screensaver written by Qt5.        
The screensaver supports biometric auhentication which is   
provided by biometric-auth service.                         
                                                            
%prep
%autosetup -n %{name}-%{version} -p1

%build
cmake .
%{make_build}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=%{buildroot}

%clean
rm -rf $RPM_BUILD_ROOT

%post
set -e
chmod u+s /usr/bin/ukui-screensaver-checkpass
glib-compile-schemas /usr/share/glib-2.0/schemas/ &> /dev/null ||:

if [ -x /usr/share/kylin-system-updater/kylin-reboot-required ]; then
    #执行请求重启提示
    /usr/share/kylin-system-updater/kylin-reboot-required
fi


%files
%doc debian/copyright debian/changelog
%{_bindir}/ukui-screensaver-dialog
%{_bindir}/ukui-screensaver-backend
%{_bindir}/ukui-screensaver-command
%{_bindir}/screensaver-startup.sh
%{_bindir}/ukui-screensaver-checkpass
%{_datadir}/ukui-screensaver/*
%{_datadir}/glib-2.0/schemas/org.ukui.screensaver.gschema.xml
%{_sysconfdir}/xdg/autostart/ukui-screensaver.desktop
%{_datadir}/desktop-directories/ukui-screensaver.directory
%{_sysconfdir}/xdg/menus/ukui-screensavers.menu
%{_sysconfdir}/pam.d/ukui-screensaver-qt
%{_prefix}/lib/ukui-screensaver/ukui-screensaver-default
%{_prefix}/lib/ukui-screensaver/screensaver-focus-helper
%{_prefix}/lib/libukss.so
%{_prefix}/lib/ukui-screensaver/libscreensaver-default.so
%{_prefix}/lib/ukui-screensaver/set4kScale
%{_datadir}/applications/ukui-screensaver-dialog.desktop
%{_includedir}/ukui-screensaver/screensaverplugin.h


%changelog
* Tue Sep 03 2024 peijiankang <peijiankang@kylinos.cn> - 4.0.0.0-2
- add 0001-fix-ukui-screensaver-4.0.0.0-about-ukui-screensaver-qt.patch

* Tue Apr 02 2024 huayadong <huayadong@kylinos.cn> - 4.0.0.0-1
- update version to 4.0.0.0

* Mon Sep 04 2023 peijiankang <peijiankang@kylinos.cn> - 3.1.1-6
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add patch3:add-switchuser-no-limits-in-ukui-screensaver.patch

* Wed Jun 14 2023 peijiankang <peijiankang@kylinos.cn> - 3.1.1-5
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: disable Suspend and Sleep of ukui-screensaver

* Thu May 25 2023 peijiankang <peijiankang@kylinos.cn> - 3.1.1-4
- update glib2 error

* Fri Feb 10 2023 tanyulong <tanyulong@kylinos.cn> - 3.1.1-3
- fix build compile error

* Fri Dec 9 2022 peijiankang <peijiankang@kylinos.cn> - 3.1.1-2
- fix root can not input passwd bug

* Tue Dec 6 2022 peijiankang <peijiankang@kylinos.cn> - 3.1.1-1
- update version to 3.1.1

* Mon Aug 08 2022 tanyulong<tanyulong@kylinos.cn> - 3.0.1-22
- modify username size

* Thu Aug 04 2022 tanyulong<tanyulong@kylinos.cn> - 3.0.1-21
- add and use attribute of UseHighDpiPixmaps

* Fri May 20 2022 tanyulong<tanyulong@kylinos.cn> - 3.0.1-20
- Improve the project according to the requirements of compliance improvement

* Sat Apr 02 2022 tanyulong <tanyulong@kylinos.cn> - 3.0.1-19
- add yaml file

* Mon Mar 28 2022 huayadong <huayadong@kylinos.cn> - 3.0.1-18
- Before adding the lock screen, determine whether the lock screen has been activated, and prevent the login password box from being input when multiple lock screens are activated.

* Fri Dec 10 2021 huayadong <huayadong@kylinos.cn> - 3.0.1-17
- add patch15: 0001-insert-the-monitor-after-placement-and-the-screensav.patch

* Thu Dec 09 2021 huayadong <huayadong@kylinos.cn> - 3.0.1-16
- add patch14: fix-root-can-not-input-passwd-bug.patch

* Thu Dec 09 2021 huayadong <huayadong@kylinos.cn> - 3.0.1-15
- add patch13:fix-root-user-had-not-face-icon.patch

* Tue Dec 07 2021 tanyulong <tanyulong@kylinos.cn> - 3.0.1-14
- Update changelog package version

* Fri Dec 03 2021 tanyulong <tanyulong@kylinos.cn> - 3.0.1-13
- add build requires packages

* Thu Dec 02 2021 tanyulong <tanyulong@kylinos.cn> - 3.0.1-12
- Add .gitattriutes files

* Wed Nov 10 2021 tanyulong <tanyulong@kylinos.cn> - 3.0.1-11
- Add opencv compilation dependency

* Fri Nov 05 2021 tanyulong <tanyulong@kylinos.cn> - 3.0.1-10
- there is only one user, the switch user button is not displayed

* Tue Nov 02 2021 tanyulong <tanyulong@kylinos.cn> - 3.0.1-9
- Screen saver add x11bypasswindowmanager attribute

* Mon Nov 1  2021 tanyulong <tanyulong@kylinos.cn> - 3.0.1-8
- When the screen saver mode is set to single, but the screen saver is empty, the problem of segfault

* Sat Oct 30 2021 tanyulong <tanyulong@kylinos.cn> - 3.0.1-7
- Add fractional scaling parameters

* Sat Oct 30 2021 tanyulong <tanyulong@kylinos.cn> - 3.0.1-6
- Add sentence author info

* Fri Oct 29 2021 tanyulong <tanyulong@kylinos.cn> - 3.0.1-5
- No the file .gitmodules,so remove in debian/copyright file

* Thu Oct 28 2021 tanyulong <tanyulong@kylinos.cn> - 3.0.1-4
- Fix a return type error

* Wed Jan 13 2021 lvhan <lvhan@kylinos.cn> - 3.0.1-3
- 0002-fix-ukui-screensaver-dialog-lock.patch

* Wed Dec 9 2020 lvhan <lvhan@kylinos.cn> - 3.0.1-2
- 0001-fix-icon-misplaced.patch

* Mon Oct 26 2020 douyan <douyan@kylinos.cn> - 3.0.1-1
- update to upstream version 3.0.0-1+1026

* Thu Jul 9 2020 douyan <douyan@kylinos.cn> - 2.1.1-1
- Init package for openEuler
